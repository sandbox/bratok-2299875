<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\msform;


class MsFormCache{
  protected $_type = null;
  protected $_data = null;
  
  function __construct($name, $data = null) {
    $this->_type = __CLASS__;
    $this->_name = $name;
    
    if ($data){
      $this->_data = $data;
    }
    else {
      ctools_include('object-cache');
      $this->_data = ctools_object_cache_get($this->_type, $this->_name);
    }
  }
  
  
  function set($name, $value){
    $this->_data[$name] = $value;
    return $this;
  }
  
  function get($name){
    return isset($this->_data[$name])?$this->_data[$name]:null;
  }
//  function __set($name, $value) {
//    if ($name == '_data'){
//      $this->_data = $value;
//    }
//    else {
//      $this->_data[$name] = $value;
//    }
//  }
  
//  function __get($name) {
//    return isset($this->_data[$name])?$this->_data[$name]:null;
//  }
  
  function save(){
    ctools_include('object-cache');
    ctools_object_cache_set($this->_type, $this->_name , $this->_data);
  }
  
  function clear(){
    ctools_include('object-cache');
    ctools_object_cache_clear($this->_type, $this->_name);
  }
}

class MsForm{
  
  public $step = null;
  public $state = null;
  public $options = null;
  
  protected $_id = null;
  
  function __construct($id, $options = null, &$state = null) {
    $this->id = $id;
//    // 
//    $form_info['id'] = 'msform'; 
//    if (!$options){
//      
//    }
//    
//    if ($state['modal'])
//    
//    $this->_optionsDefault();
//    $form_info  = ctools_wizard_defaults($options);
    $options['id'] = 'msform';
    $this->options = $options;
    $this->options['next callback']   = 'Drupal\msform\msform_next';
    $this->options['back callback']   = 'Drupal\msform\msform_back';
    $this->options['return callback'] = 'Drupal\msform\msform_return';
    $this->options['finish callback'] = 'Drupal\msform\msform_finish';
    $this->options['cancel callback'] = 'Drupal\msform\msform_cancel';
    $this->state = $state;
    
    if (isset($options['cached vars']) && is_array($options['cached vars'])){
      $this->cachedVars = $options['cached vars'];
    }
    $this->state['msform'] = $this;
  }
  
  function _optionsDefault(){
    $form_info  = ctools_wizard_defaults($options);
  }
  
  /**
   * 
   * @param string $name
   * @deprecated since version number
   */
  function cacheName(string $name = null){
    if ($this->_cache_name && $name){
      $this->cacheClear();
    }
    elseif ($name){
      $this->_cache_name = $name;
    }
    else {
      $this->_cache_name;
    }
  }
  /**
   * 
   * @param type $data
   * @return type
   * @todo 
   * Удалить __CLASS__ так как в случае если открыть несколько 
   * пошаговых диалогов они презетрут друг друга  
   */
  function cache($data = null){
    if ($data){
      if (isset($this->_cache)){
        $this->cache->clear();
        $this->cache = null;
      }
      $this->_cache = new MsFormCache($this->id, $data);
    }
    else if (!isset($this->_cache)) {
      $this->_cache = new MsFormCache($this->id);
    }
    return $this->_cache;
  }
  
  
  function form($step = null){
    
    if (!isset($step) || !isset($this->options['order'][$step])){
      $this->cache()->clear();
      $step = array_keys($this->options['order'])[0];
    }
//    $this->state['ajax'] = (bool)$ajax;
    $this->step  = $step;
    ctools_include('wizard');
    $form = ctools_wizard_multistep_form($this->options, $step, $this->state);
    return $form; 
  }
  
  function next(&$form_state) {
    // Много шаговая форма может использовать один и тотже 
    // конструктор на кажном шаге, т.е. form_id  будет всегда одинаков, 
    // что бы система форм не дергала постоянно одну и туже форму из кэша
    //  на следующем шаге и не происходило зацикливания, нам нужно обнулить 
    //  входные параметры $form_state['input']['form_id'], $form_state['input']['form_build_id']. 
    //  Т.е. как бы сказать системе, что форма новая 
    $form_state['input']['form_id'] = null;
    $form_state['input']['form_build_id'] = null;
    if (isset($this->cachedVars)){
      foreach($this->cachedVars as $var_name){
        $this
          ->cache()
          ->set($var_name, $form_state[$var_name]);
      }
      $this->cache()->save();
    }
  }

  function ret(&$form_state) {
    $this->cache()->clear();
  }

  function back(&$form_state) {
    $form_state['input']['form_id'] = null;
    $form_state['input']['form_build_id'] = null;
  }

  function cancel(&$form_state) {
    $this->cache()->clear();
  }

  function finish(&$form_state) {
    $this->cache()->clear();
  }
}


function msform_next(&$form_state) {
  $form_state['msform']->next($form_state);
}

function msform_return(&$form_state) {
  $form_state['msform']->ret($form_state);
}

function msform_back(&$form_state) {
  $form_state['msform']->back($form_state);
}

function msform_cancel(&$form_state) {
  $form_state['msform']->cancel($form_state);
}

function msform_finish(&$form_state) {
  $form_state['msform']->finish($form_state);
  
}




 